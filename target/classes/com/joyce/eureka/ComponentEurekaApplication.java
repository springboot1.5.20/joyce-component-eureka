package com.joyce.eureka;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaServer
@ComponentScan("com.joyce")
public class ComponentEurekaApplication {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ComponentEurekaApplication.class);
	
	public static void main(String[] args) {
		SpringApplication.run(ComponentEurekaApplication.class, args); 
		LOG.info("Hello World!");
	}
}
